package be.liantis.zoscripturabatchperformancetest

import com.rabbitmq.client.AMQP
import com.rabbitmq.client.Channel
import org.apache.http.client.HttpClient
import org.apache.http.client.methods.HttpPost
import org.apache.http.conn.ssl.NoopHostnameVerifier
import org.apache.http.impl.client.HttpClients
import org.apache.http.ssl.SSLContextBuilder
import java.util.*

class Main


/*
 * VARIABLES
 */

val sc = Scanner(System.`in`)

val rabbitUsername = "zo-scriptura-batch-performance-test"
val rabbitPassword = "zo-scriptura-batch-performance-test"
val rabbitPort = 5672
val rabbitAddresses = mapOf(
        "local" to "localhost:35672:/",
        "dev" to "mbbd.liantis.be:$rabbitPort:ZG-General",
        "test" to "mbbt.liantis.be:$rabbitPort:ZG-General",
        "accept" to "mbba.liantis.be:$rabbitPort:ZG-General"
)

val ventourisBatchAddresses = mapOf(
        "local" to "http://localhost:8284/zop/integration/loket",
        "dev" to "http://zebd.liantis.be:8284/zop/integration/loket",
        "test" to "http://zebd.liantis.be:8284/zop/integration/loket",
        "accept" to "http://zebd.liantis.be:8284/zop/integration/loket"
)

val vapenzoBatchAddresses = mapOf(
        "local" to "localhost:8295",
        "dev" to "zebd.liantis.be:16126",
        "test" to "zebd.liantis.be:16126",
        "accept" to "zebd.liantis.be:16126"
)


/*
 * MAIN FUNCTIONS
 */

fun main(args: Array<String>) {
    try {
        println("Launcing scriptura batch performance test")

        println(">Environment: ")
        val env = sc.next()
        if (!rabbitAddresses.containsKey(env)) {
            println("Error: invalid environment")
            System.exit(1)
        }

        println(">Batch size: ")
        val size = sc.nextInt()

        println(">Document domain: ")
        val domain = sc.next()

        println(">Document type: ")
        val type = sc.next()

        sendDocumentStoreRequests(env, size, domain, type)

        println("Waiting for $size seconds")
        Thread.sleep(1_000L * size)

        startBatch(env, domain, type)

    } catch (e: Exception) {
        e.printStackTrace()
    } finally {
        println("Exiting scriptura batch performance test")
        System.exit(0)
    }
}

fun sendDocumentStoreRequests(env: String, size: Int, domain: String, type: String) {
    println("Sending $size document store requests")

    val document = Main::class.java.getResourceAsStream("/be/liantis/zoscripturabatchperformancetest/doc.pdf").readBytes()

    IntRange(1, size).forEach {
        channel(env).basicPublish(
                "loket.scriptura.store.request",
                "zo-scriptura-integration-service.loket.scriptura.store.request",
                AMQP.BasicProperties.Builder()
                        .contentType("text/plain")
                        .headers(documentMetadata(domain, type, it))
                        .build(),
                Base64.getEncoder().encode(document)
        )
    }

}

fun startBatch(env: String, domain: String, type: String) {
    val url = "${ventourisBatchAddresses[env]}/batches/$domain/types/$type"

    println("Sending batch request to $url")

    val client = client()
    val request = HttpPost(url)
    val response = client.execute(request)
    println(response.statusLine)

}


/*
 * UTIL FUNCTIONS
 */

fun channel(env: String): Channel {
    val address = rabbitAddresses[env]!!
    val cf = com.rabbitmq.client.ConnectionFactory()
    cf.host = address.split(":")[0]
    cf.port = Integer.parseInt(address.split(":")[1])
    cf.virtualHost = address.split(":")[2]
    cf.username = rabbitUsername
    cf.password = rabbitPassword
    return cf.newConnection(rabbitUsername).createChannel()
}

fun client(): HttpClient {
    return HttpClients.custom()
            .setSSLContext(SSLContextBuilder()
                    .loadTrustMaterial(null) { _, _ -> true }.build())
            .setSSLHostnameVerifier(NoopHostnameVerifier())
            .build()
}

fun documentMetadata(domain: String, type: String, index: Int) = mapOf(
        "xIdentifier" to "DOS-ZS-22076960",
        "ESBFilename" to "VENDOC-ZS-9587057.pdf",
        "xRelatedContentTargetName" to "DOS-ZS-22076960",
        "domein" to domain,
        "xFunctionalIdentifier" to "94102914955",
        "xCategoryID" to "Batch1Year",
        "dDocAuthor" to "VENTOURIS",
        "xPublisher" to "VENTOURIS",
        "ZEN_SCRIPT_UUID" to "ZEN_SCRIPT_UUID_d65d4c4b-6431-4268-b77d-0bc24f227c1f",
        "xDossierAdministration" to "030",
        "correlationId" to "scriptura-batch-performance-test.$index.${System.currentTimeMillis()}",
        "xContentCodeDetail" to "2018",
        "dSecurityGroup" to "SVMB_Inning",
        "xDossierSuccursale" to "BRUS_InningZS",
        "xDossierResponsible" to "Lehouck Florine (050) 474339",
        "xFunctionalIdentifierType" to "rijksregisternummer",
        "documentType" to type,
        "datumAanmaak" to "2018-01-13",
        "xDossierName" to "Putin Vladimir",
        "xDossierAddress" to "Kremlin, Rode Plein 01",
        "xContentCode" to "R011201",
        "xOrganization" to "InningZS",
        "template" to "FiscaalAttest",
        "DropSubFolder" to """$domain\\$type\\7""",
        "dDocName" to "performance-test-$index"
)
